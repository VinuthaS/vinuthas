package com.swings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import javax.swing.*;

public class RegisterPage extends JFrame
{
	JLabel Nlabel,Alabel,Phlabel,Elabel,Slabel,Clabel;
    JTextField NText,AText,PHtext,Etext,Stext,Ctext;
    JButton btn;
    
    RegisterPage()
    {
 	   setVisible(true);
   	 setSize(1000,700);
   	 setLayout(null);
   	 
 	 Nlabel=new JLabel();
 	 Nlabel.setText("Enter the name");
 	 Alabel=new JLabel();
 	 Alabel.setText("Enter the Address");
 	 Phlabel=new JLabel();
 	 Phlabel.setText("Enter the phone");
 	 Elabel=new JLabel();
 	 Elabel.setText("Enter the Email Id");
 	 Slabel=new JLabel();
 	 Slabel.setText("Enter the State name");
 	 Clabel=new JLabel();
 	 Clabel.setText("Enter the Country name");
 	 
 	 Nlabel.setBounds(80,70,200,30);//x axis, y axis, width, height 
 	 Alabel.setBounds(80,110,200,30);
 	 Phlabel.setBounds(80, 150, 200, 30);
 	 Elabel.setBounds(80, 190, 200, 30);
 	 Slabel.setBounds(80, 230, 200, 30);
 	 Clabel.setBounds(80, 270, 200, 30);
 	 
 	 NText=new JTextField(10);
 	 NText.setBounds(300,70,200,20);
 	 
 	 AText=new JTextField(10);
	 AText.setBounds(300,110,200,20);
 	 
 	 PHtext=new JTextField(10);
	 PHtext.setBounds(300,150,200,20);
	 
	 Etext=new JTextField(10);
	 Etext.setBounds(300,190,200,20);
	 
	 Stext=new JTextField(10);
	 Stext.setBounds(300,230,200,20);
	 
	 Ctext=new JTextField(10);
	 Ctext.setBounds(300,270,200,20);
	 
 	 btn=new JButton("Register");
 	 btn.setBounds(250,430,100,30);
 	//btn.setBounds(450,630,200,50);
 	 
 	 add(Nlabel);
 	 add(Alabel);
   	add(Phlabel);
	 add(Elabel);
	 add(Slabel);
	 add(Clabel);
	 
	 
 	 add(NText);
 	 add(AText);
 	add(PHtext);
 	add(Etext);
 	add(Stext);
 	add(Ctext);
 	
 	
 	 add(btn);
 	setVisible(true);
 	 setSize(1000,700);
 	 setLayout(null);
 	 
 	 
 	 btn.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e1) {
			// TODO Auto-generated method stub
			if(e1.getSource()==btn)
			{
				int x=0;
			int count=0;
			int temp=0;
			String rname=NText.getText();
			String raddr=AText.getText();
			String rph=PHtext.getText();
			String remail=Etext.getText();
			String rstate=Stext.getText();
			String rcountry=Ctext.getText();
			int num=Integer.parseInt(rph);
			
			Connection con=null;
        	PreparedStatement pstmt=null;
        	ResultSet rs=null;
        	try{
	    		Class.forName("com.mysql.jdbc.Driver");
	    		con=DriverManager.getConnection("jdbc:mysql://localhost:3306?user=root&password=tiger");
	    		pstmt=con.prepareStatement("insert into dblogin.registerpage values (?,?,?,?,?,?)");
	    		pstmt.setString(1,rname);
	    		pstmt.setString(2,raddr);
	    		pstmt.setInt(3,num);
	    		pstmt.setString(4,remail);
	    		pstmt.setString(5,rstate);
	    		pstmt.setString(6,rcountry);
	    		
	    		int k=pstmt.executeUpdate();
	    		x++;
	    		
	    		//int num=Integer.parseInt(rph);
   			 temp=num;
   			while(num!=0)
   			{
   				
   				num=num/10;
   				count++;
   			}
   		System.out.println(count);
	    		
	    		if(rname.equals("")||raddr.equals("")||remail.equals("")||rph.equals(""))
	    		{
	    			JOptionPane.showMessageDialog(null, "Please Enter the Required Field");
	    			
	    		}
	    		
	    		
	    		
	    		
	    		else if(x>0)
	    		{
	    			JOptionPane.showMessageDialog(null, "Registration successful");
	    		}
	    		
	    		
        	
        	}
        	catch (ClassNotFoundException | SQLException e) {
    			e.printStackTrace();
        	
		}
        	finally
    		{

    			if(rs!=null)
    			{
    				try {
    					rs.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
    			}
    			if(pstmt!=null)
    			{
    				try {
    					pstmt.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
    			}
    			if(con!=null)
    			{
    				try {
    					con.close();
    				} catch (SQLException e) {
    					e.printStackTrace();
    				}
    			}
    		}
			}
		}
	});
    }
   
}
